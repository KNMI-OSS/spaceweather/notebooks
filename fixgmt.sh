cd /Applications/GMT-6.5.0.app/Contents/Resources/lib
for dylib in $(ls *.dylib); do
	otool -L $dylib | awk -v lib=$dylib '/^[[:blank:]]*@executable_path/ {printf("install_name_tool -change %s @loader_path/%s %s\n", $1, substr($1, 18), lib)}' | sh
done

for libso in $(ls gmt/plugins/*.so); do
	otool -L $libso | awk -v lib=$libso '/^[[:blank:]]*@executable_path/ {printf("install_name_tool -change %s @loader_path/../../%s %s\n", $1, substr($1, 18), lib)}' | sh
done
