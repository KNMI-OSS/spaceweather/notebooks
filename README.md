# notebooks

A place to collect KNMI space weather stand-alone notebooks


# Issues with installation of dependencies:

## macOS:

### Installing PyHDF and hdf4 on macOS

The PyHDF library can be used to access HDF4 files, used for storing ACE science data. PyHDF
requires the HDF4 libraries to be installed, which can be done using MacPorts:
``
When the HDF4 library is installed using MacPorts (recommended) or self-installed, it is necessary to
instruct pipenv where to find this library and its header file. This can be done by
passing LDFLAGS and CFLAGS on the command-line:
```
sudo port install hdf4
LDFLAGS='-L/opt/local/lib' CFLAGS='-I/opt/local/include' pipenv install pyhdf
```

### Installing GMT and PyGMT using pipenv
PyGMT can be installed along with GMT using conda, by following the [instructions on the website](https://www.pygmt.org/latest/install.html).

However, when using pipenv, GMT has to be installed separately on the system, outside of the Python environment. 
On macOS, GMT can be easily installed from its [package](https://www.generic-mapping-tools.org/download/), but it might be necessary
to modify the libraries, to make pygmt be able to use them. This issue contains a short script to do so:
https://github.com/GenericMappingTools/gmt/issues/1930

## Other OSes:
The notebooks have not yet been tested with other operating systems.
