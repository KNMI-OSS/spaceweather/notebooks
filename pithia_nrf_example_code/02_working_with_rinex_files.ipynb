{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "edd7075f",
   "metadata": {},
   "source": [
    "# 02 Working with RINEX files\n",
    "\n",
    "In addition to the ISMR files from example 1, the receivers also generate RINEX files. These Receiver Independent Exchange Format (RINEX) files generally come in two types: observation files containing the observation data (e.g. GPS's L1C signal) sorted by satellite and timestamp, and navigation files containing the broadcast navigation data records. Both types are divided into a header section and a data section. Given the large file size of some rinex files, they can be compressed (Hatanake file compression).\n",
    "\n",
    "The Python package `georinex` is used to convert the RINEX file into a `xarray.Dataset` which can be saved as a NetCDF4 file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d8b02d97",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import georinex as gr\n",
    "import xarray as xr"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "67907cea",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the rinex files\n",
    "ds_obs = gr.load('SABA2690.21d')\n",
    "ds_nav = gr.load('SABA2690.21p')\n",
    "print(type(ds_obs))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c7e9f919",
   "metadata": {},
   "source": [
    "This takes quite some time. This process can be speeded up by creating a NetCDF file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3d18e5c6",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds_obs.to_netcdf('SABA2690.21d.nc')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "894271e5",
   "metadata": {},
   "source": [
    "This can now be loaded directly into an `xarray.Dataset`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d3a1296d",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds_obs = xr.open_dataset('SABA2690.21d.nc')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5b8dfa58",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(ds_obs)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e85b1165",
   "metadata": {},
   "source": [
    "The navigation file contains different data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "40155046",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(ds_nav)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9f752a1d",
   "metadata": {},
   "source": [
    "Among these variables are parameters to determine the position of the satellite. Depending on the constellation a different system is used. The GPS navigation message contains orbit information (similar to Kepler orbits) which is  broadcasted every 2 hours:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6ba69aa6",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds_nav_g5 = ds_nav.sel(sv='G05') # Take GPS satellite #5 for example\n",
    "notnullarray = ds_nav_g5.Crs.notnull().data # This dimension contains 2459 elements, mostly NaNs\n",
    "print(ds_nav_g5.time[notnullarray].data) # Select the non-NaN values"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6302be6b",
   "metadata": {},
   "source": [
    "The orbit parameters in the navigation message can be used to calculate the ECEF xyz coordinates of a GPS satellite at time t. For this a function is created:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "62c9cbf8",
   "metadata": {},
   "outputs": [],
   "source": [
    "def calculate_xyz_in_ecef(t,    toe,    M0,       roota, deltan, ecc, omega,\n",
    "                          cuc,  cus,    crc,      crs,   cic,    cis, i0,\n",
    "                          idot, Omega0, Omegadot):\n",
    "    # See A6 on page 68 in https://files.igs.org/pub/data/format/rinex305.pdf\n",
    "    # to see what's inside a GPS navigation rinex file\n",
    "    # See page 106 in https://www.gps.gov/technical/icwg/IS-GPS-200M.pdf\n",
    "\n",
    "    # Earth's universal gravitational parameter, m^3/s^2\n",
    "    GM = 3.986004418e14\n",
    "\n",
    "    # earth rotation rate, rad/s\n",
    "    Omegae_dot = 7.2921151467e-5 # note, different from Omegadot\n",
    "\n",
    "    # speed of light\n",
    "    c = 0.299792458e9\n",
    "\n",
    "    # Now compute satellite position\n",
    "    A = roota * roota\n",
    "\n",
    "    # time elapsed since toe which is Time of Ephemeris (seconds of GPS week)\n",
    "    tk = t #- toe\n",
    "\n",
    "    # mean anomaly at t\n",
    "    M = M0 + (np.sqrt(GM / A**3) + deltan) * tk\n",
    "\n",
    "    # iterative solution for E\n",
    "    E_old = M\n",
    "    dE = 1\n",
    "    while (dE > 1e-11):\n",
    "        E = M + ecc * np.sin(E_old)\n",
    "        dE = abs(E-E_old)\n",
    "        E_old = E\n",
    "\n",
    "    # true anomaly\n",
    "    v = 2 * np.arctan(np.sqrt((1 + ecc) / (1 - ecc)) * np.tan(E / 2))\n",
    "\n",
    "    # argument of latitude\n",
    "    phi = omega + v\n",
    "    phi = phi % (2 * np.pi) \n",
    "\n",
    "    # second harmonic perturbations\n",
    "    deltau = cus * np.sin(2 * phi) + cuc * np.cos(2 * phi)\n",
    "    deltar = crs * np.sin(2 * phi) + crc * np.cos(2 * phi)\n",
    "    deltai = cis * np.sin(2 * phi) + cic * np.cos(2 * phi)\n",
    "\n",
    "    # corrected argument of latitude\n",
    "    u = phi + deltau\n",
    "\n",
    "    # corrected radius\n",
    "    r = A * (1 - ecc * np.cos(E)) + deltar\n",
    "\n",
    "    # corrected inclination\n",
    "    i = i0 + idot * tk + deltai\n",
    "\n",
    "    # positions in orbital plane\n",
    "    xacc = r * np.cos(u)\n",
    "    yacc = r * np.sin(u)\n",
    "\n",
    "    # longitude of ascending node\n",
    "    Omega = Omega0 + (Omegadot - Omegae_dot) * tk - Omegae_dot * toe\n",
    "\n",
    "    # earth-fixed coordinates\n",
    "    x = xacc * np.cos(Omega) - yacc * np.cos(i) * np.sin(Omega)\n",
    "    y = xacc * np.sin(Omega) + yacc * np.cos(i) * np.cos(Omega)\n",
    "    z = yacc * np.sin(i)\n",
    "\n",
    "    return [x, y, z]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "887258ba",
   "metadata": {},
   "source": [
    "Find the ECEF position of GPS satellite G05 one hour after the navigation message was distributed at 2021-09-26T12:00:00.000000000"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b180cfcf",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds_nav_used = ds_nav_g5.sel(time = '2021-09-26T12:00:00.000000000')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bd127fd3",
   "metadata": {},
   "outputs": [],
   "source": [
    "toe = ds_nav_used['Toe'].data\n",
    "M0 = ds_nav_used['M0'].data\n",
    "roota = ds_nav_used['sqrtA'].data\n",
    "deltan = ds_nav_used['DeltaN'].data\n",
    "ecc = ds_nav_used['Eccentricity'].data\n",
    "omega = ds_nav_used['omega'].data\n",
    "cuc = ds_nav_used['Cuc'].data\n",
    "cus = ds_nav_used['Cus'].data\n",
    "crc = ds_nav_used['Crc'].data\n",
    "crs = ds_nav_used['Crs'].data\n",
    "cic = ds_nav_used['Cic'].data\n",
    "cis = ds_nav_used['Cis'].data\n",
    "i0 = ds_nav_used['Io'].data\n",
    "idot = ds_nav_used['IDOT'].data\n",
    "Omega0 = ds_nav_used['Omega0'].data\n",
    "Omegadot = ds_nav_used['OmegaDot'].data\n",
    "\n",
    "t = 3600 # seconds in 1 hour\n",
    "x,y,z = calculate_xyz_in_ecef(t, toe, M0, roota, deltan, ecc, omega,\n",
    "                              cuc, cus, crc, crs, cic, cis, i0, idot, Omega0, Omegadot)\n",
    "print(x,y,z)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "967db41b",
   "metadata": {},
   "source": [
    "Now that the coordinates of the satellite can be calculated, the pierce points through the ionospheric screen can also be determined. For this another function is needed:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a15bb877",
   "metadata": {},
   "outputs": [],
   "source": [
    "def calculate_pierce_points(azimuth, elev, lon, lat, layer_height = 350):\n",
    "    # all in radians\n",
    "    # https://doi.org/10.1007/s10509-018-3303-4\n",
    "    Re = 6378.1363 * 10 ** 3 # Earth radius in meter\n",
    "    h = layer_height * 10 ** 3\n",
    "    K = (Re / (Re + h)) * np.cos(elev)\n",
    "    psi_pp = (np.pi / 2) - elev - np.arcsin(K)\n",
    "    lat_pp = np.arcsin(np.sin(lat) * np.cos(psi_pp) + np.cos(lat) * np.sin(psi_pp) * np.cos(azimuth))\n",
    "    lon_pp = lon + np.arcsin((np.sin(psi_pp) * np.sin(azimuth)) / np.cos(lat_pp))\n",
    "\n",
    "    # return in radians\n",
    "    return [lat_pp, lon_pp]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "74ae18e8",
   "metadata": {},
   "source": [
    "Calculate the ionspheric pierce points using the receiver location and GPS coordinates:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b63cb61f",
   "metadata": {},
   "outputs": [],
   "source": [
    "from astropy.coordinates import AltAz, EarthLocation\n",
    "import astropy.units as units\n",
    "from astropy.time import Time"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1bdc7de9",
   "metadata": {},
   "outputs": [],
   "source": [
    "SABA_x = 2737737.3007 # from the header of the observation rinex file\n",
    "SABA_y = -5429882.3492\n",
    "SABA_z = 1918480.5016\n",
    "\n",
    "obs_time = np.datetime64('2021-09-26T13:00:00.000000000')\n",
    "\n",
    "# Calculate azimuth and altitude at receiver position\n",
    "sat_pos = EarthLocation.from_geocentric(x, y, z, unit=units.meter)\n",
    "receiver_pos = EarthLocation.from_geocentric(SABA_x, SABA_y, SABA_z, unit=units.meter)\n",
    "gps_altaz = sat_pos.get_itrs(obstime=obs_time).transform_to(AltAz(location=receiver_pos, obstime=obs_time))\n",
    "\n",
    "# Calculate ionospheric pierce points\n",
    "azimuth = gps_altaz.az.rad\n",
    "elev = gps_altaz.alt.rad\n",
    "receiver_lon = receiver_pos.lon.rad\n",
    "receiver_lat = receiver_pos.lat.rad\n",
    "lat_pp, lon_pp = calculate_pierce_points(azimuth, elev, receiver_lon, receiver_lat)\n",
    "\n",
    "print(np.degrees(lat_pp), np.degrees(lon_pp))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "625a6aa7",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
