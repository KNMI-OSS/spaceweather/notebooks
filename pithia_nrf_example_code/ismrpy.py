# See: https://github.com/dinilbose/ismrpy/blob/master/ismrpy/main.py
# The module helps to read basic ismr file and convert it into panda dataframe for quick and easy analysis
import pandas as pd
import numpy as np
import datetime
from astropy.utils import iers
import time
import os

_GPSEPOCH = datetime.datetime.strptime("1980-01-06 00:00:00", datetimeformat)

def generate_hdf5_from_ismr_in_period(filename, directory, start, end, lat=None, lon=None, layer_height=350):
    start_day_of_year = start.timetuple().tm_yday
    end_day_of_year = end.timetuple().tm_yday

    store = pd.HDFStore(filename, mode='w')

    files = sorted(glob.glob(directory + "/*/*"))
    for file in files:

        fileday = pd.to_datetime(file.split("/")[-2], format="%y%j")
        if fileday > start and fileday < end:
            try:
                ismr_df = read_ismr(ismr_file_path, lat=lat, lon=lon, layer_height=layer_height)
                #store.append('ismr', ismr_df, data_columns=True)
            except:
                print('Not available or something went wrong reading file:', ismr_file_path)
                continue

            print(ismr_file_path)

    return store

def read_ismr(filename='', lat=None, lon=None, layer_height=350):
    """"
    Reads ismr and provide with panda data frame
        filename= Name of the file
    If provided with lat and lon, program  Calculate Ionospheric pierce points and Slant TEC(Stec)
        lat= latitude of station
        lon=longitude of station
        layer_height=Height of ionospheric layer in kilometers (default 350 KM)
    """
    print(filename, end='\r')
    data = pd.read_csv(filename, names=ismr_column_converters.keys(), parse_dates=[['GPS_Week_Number', 'GPS_Time_Week']])#,
                       #date_parser=_weeksecondstoutc, converters=ismr_column_converters)
    #data = pd.read_csv(filename, names=ismr_column_converters.keys(), parse_dates=[['GPS_Week_Number', 'GPS_Time_Week']],
    #                   date_parser=_weeksecondstoutc, skipinitialspace=True, dtype=np.float)

    data = data.rename(columns={'GPS_Week_Number_GPS_Time_Week': 'Time'})
    data = data.set_index(['Time'], drop=False)
    #data['constellation'] = data.SVID.apply(_sat_constellation)

#     if not lat == None and not lon == None:
#         # Calculate ionospheric pierce points
#         # See https://doi.org/10.1007/s10509-018-3303-4
#         elev = np.radians(data.Elevation)
#         azimuth = np.radians(data.Azimuth)
#         lat = np.radians(lat)
#         lon = np.radians(lon)

#         Re = 6378.1363 * 10 ** 3 # Earth radius in meter
#         h = layer_height * 10 ** 3
#         K = (Re / (Re + h)) * np.cos(elev)
#         psi_pp = (np.pi / 2) - elev - np.arcsin(K)
#         lat_pp = np.arcsin(np.sin(lat) * np.cos(psi_pp) + np.cos(lat) * np.sin(psi_pp) * np.cos(azimuth))
#         lon_pp = lon + np.arcsin((np.sin(psi_pp) * np.sin(azimuth)) / np.cos(lat_pp))

#         lat_pp = np.degrees(lat_pp)
#         lon_pp = np.degrees(lon_pp)

#         data['lat_pp'] = lat_pp
#         data['lon_pp'] = lon_pp

        # Calculate vTEC
        # See chapter 6.3.4 in ISBN 978-3-319-42928-1
        # https://doi.org/10.1007/978-3-319-42928-1
        # vtec = data.TEC_TOW * np.sqrt(1 - K**2)
        # data['vTEC'] = vtec

    return data

def _strip_and_make_float(text):
    try:
        return np.float64(text.strip())
    except AttributeError:
        print("AttributeError:", text)
        return text

def _weeksecondstoutc(gpsweek, gpsseconds):
    """"
    Convert gpsweek and gpseconds to time (GPS, no UTC)
    """
    gpsweek = float(gpsweek)
    gpsseconds = float(gpsseconds)
    # Now correct for the leap seconds
    # See: http://hpiers.obspm.fr/eop-pc/index.php?index=TAI-UTC_tab&lang=en
    # IERS = International Earth Rotation and Reference Systems Service
    # ERFA = Essential Routines for Fundamental Astronomy
    # ERFA is based on SOFA (Standard of Fundamental Astronomy)
    # which was esthablished by the IAU (International Astronomical Union)
    #total_leapseconds = iers.LeapSeconds().from_erfa()[-1][2]
    #leapseconds_since_1980 = 19 # see list at hpiers.obspm.fr
    #leapseconds = total_leapseconds - leapseconds_since_1980 
    # See: http://hpiers.obspm.fr/eop-pc/index.php?index=TAI-UTC_tab&lang=en
    #datetimeformat = "%Y-%m-%d %H:%M:%S"
    #leapseconds = 0
    #elapsed = datetime.timedelta(days=(gpsweek * 7), seconds=(gpsseconds + leapseconds))
    return _GPSEPOCH + pd.to_timedelta(days=gpsweek * 7) + pd.to_timedelta(seconds=gpsseconds)

def _sat_constellation(x):
    if 1 <= x <= 37:
        sv = 'GPS' # space vehicle
    elif 38 <= x <= 62:
        sv = 'GLONASS'
    elif 71 <= x <= 102:
        sv = 'GALILEO'
    elif 107 <= x <= 119:
        sv = 'MSS'
    elif 120 <= x <= 140:
        sv = 'SBAS'
    elif 141 <= x <= 172:
        sv = 'BEIDOU'
    elif 181 <= x <= 187:
        sv = 'QZSS'
    elif 191 <= x <= 197:
        sv = 'IRNSS'
    else:
        sv = "Other"
    return sv

ismr_column_converters = {'GPS_Week_Number' : _strip_and_make_float,#None,
               'GPS_Time_Week' : _strip_and_make_float,#None,
               'SVID' : _strip_and_make_float,#None,
               'Value_RxState_field' : _strip_and_make_float,#None,
               'Azimuth' : _strip_and_make_float,#None, # Not very precise (degree integers)
               'Elevation' : _strip_and_make_float,#None, # Not very precise (degree integers)
               'Average_Sig1_C_N0' : _strip_and_make_float,
               'Total_S4_Sig1' : _strip_and_make_float,
               'Correction_total_S4_Sig1' : _strip_and_make_float,
               'Phi01_Sig1_1' : _strip_and_make_float,
               'Phi03_Sig1_3' : _strip_and_make_float,
               'Phi10_Sig1_10' : _strip_and_make_float,
               'Phi30_Sig1_30' : _strip_and_make_float,
               'Phi60_Sig1_60' : _strip_and_make_float,
               'AvgCCD_Sig1' : _strip_and_make_float,
               'SigmaCCD_Sig1' : _strip_and_make_float,
               'TEC_TOW_45s' : _strip_and_make_float,
               'dTEC_TOW_60s_TOW_45s' : _strip_and_make_float,
               'TEC_TOW_30s' : _strip_and_make_float,
               'dTEC_TOW_45s_TOW_30s' : _strip_and_make_float,
               'TEC_TOW_15s' : _strip_and_make_float,
               'dTEC_TOW_30s_TOW_15s' : _strip_and_make_float,
               'TEC_TOW' : _strip_and_make_float,
               'dTEC_TOW_15s_TOW' : _strip_and_make_float,
               'Sig1_lock_time' : _strip_and_make_float, # integer but contains nans, thus cast into float
               'sbf2ismr_version_number' : None,
               'Lock_time_second_frequency_TEC' : _strip_and_make_float, # integer but contains nans, thus cast into float
               'Averaged_C_N0_second_frequency_TEC_computation' : _strip_and_make_float,
               'SI_Index_Sig1' : _strip_and_make_float,
               'SI_Index_Sig1_numerator' : _strip_and_make_float,
               'p_Sig1_spectral_slope' : _strip_and_make_float,
               'Average_Sig2_C_N0' : _strip_and_make_float,
               'Total_S4_Sig2' : _strip_and_make_float,
               'Correction_total_S4_Sig2' : _strip_and_make_float,
               'Phi01_Sig2_1' : _strip_and_make_float,
               'Phi03_Sig2_3' : _strip_and_make_float,
               'Phi10_Sig2_10' : _strip_and_make_float,
               'Phi30_Sig2_30' : _strip_and_make_float,
               'Phi60_Sig2_60' : _strip_and_make_float,
               'AvgCCD_Sig2' : _strip_and_make_float,
               'SigmaCCD_Sig2' : _strip_and_make_float,
               'Sig2_lock_time' : _strip_and_make_float, # integer but contains nans, thus cast into float,
               'SI_Index_Sig2' : _strip_and_make_float,
               'SI_Index_Sig2_numerator' : _strip_and_make_float,
               'p_Sig2_phase' : _strip_and_make_float,
               'Average_Sig3_C_N0' : _strip_and_make_float,
               'Total_S4_Sig3' : _strip_and_make_float,
               'Correction_total_S4_Sig3' : _strip_and_make_float,
               'Phi01_Sig3_1' : _strip_and_make_float,
               'Phi03_Sig3_3' : _strip_and_make_float,
               'Phi10_Sig3_10' : _strip_and_make_float,
               'Phi30_Sig3_30' : _strip_and_make_float,
               'Phi60_Sig3_60' : _strip_and_make_float,
               'AvgCCD_Sig3' : _strip_and_make_float,
               'SigmaCCD_Sig3' : _strip_and_make_float,
               'Sig3_lock_time' : _strip_and_make_float, # integer but contains nans, thus cast into float,
               'SI_Index_Sig3' : _strip_and_make_float,
               'SI_Index_Sig3_numerator' : _strip_and_make_float,
               'p_Sig3_phase' : _strip_and_make_float,
               'T_Sig1_phase' : _strip_and_make_float,
               'T_Sig2_phase' : _strip_and_make_float,
               'T_Sig3_phase' : _strip_and_make_float}


"""
Format of the ISMR output file
------------------------------
Note:
GPS     - USA
GLONASS - Russia
SBAS    - international
QZSS    - Japan
BeiDou  - China
IRNSS   - India
GALILEO - Europe

  Note: "Sig1" means L1CA for GPS/GLONASS/SBAS/QZSS, L1BC for GALILEO, B1 for BeiDou and L5 for IRNSS.
        "Sig2" means L2C for GPS/GLONASS/QZSS, E5a for GALILEO, L5 for SBAS, B2 for BeiDou.
        "Sig3" means L5 for GPS/QZSS, E5b for GALILEO, B3 for BeiDou.
Col  1: WN, GPS Week Number
Col  2: TOW, GPS Time of Week (seconds)
Col  3: SVID (see numbering convention in the ’SBF Outline’ section of the Reference Guide)
Col  4: Value of the RxState field of the ReceiverStatus SBF block
Col  5: Azimuth (degrees)
Col  6: Elevation (degrees)
Col  7: Average Sig1 C/N0 over the last minute (dB-Hz)
Col  8: Total S4 on Sig1 (dimensionless)
Col  9: Correction to total S4 on Sig1 (thermal noise component only) (dimensionless)
Col 10: Phi01 on Sig1,  1-second phase sigma (radians)
Col 11: Phi03 on Sig1,  3-second phase sigma (radians)
Col 12: Phi10 on Sig1, 10-second phase sigma (radians)
Col 13: Phi30 on Sig1, 30-second phase sigma (radians)
Col 14: Phi60 on Sig1, 60-second phase sigma (radians)
Col 15: AvgCCD on Sig1, average of code/carrier divergence (meters)
Col 16: SigmaCCD on Sig1, standard deviation of code/carrier divergence (meters)
Col 17: TEC at TOW-45s (TECU), taking calibration into account (see -C option)
Col 18: dTEC from TOW-60s to TOW-45s (TECU)
Col 19: TEC at TOW-30s (TECU), taking calibration into account (see -C option)
Col 20: dTEC from TOW-45s to TOW-30s (TECU)
Col 21: TEC at TOW-15s (TECU), taking calibration into account (see -C option)
Col 22: dTEC from TOW-30s to TOW-15s (TECU)
Col 23: TEC at TOW (TECU), taking calibration into account (see -C option)
Col 24: dTEC from TOW-15s to TOW (TECU)
Col 25: Sig1 lock time (seconds)
Col 26: sbf2ismr version number
Col 27: Lock time on the second frequency used for the TEC computation (seconds)
Col 28: Averaged C/N0 of second frequency used for the TEC computation (dB-Hz)
Col 29: SI Index on Sig1: (10*log10(Pmax)-10*log10(Pmin))/(10*log10(Pmax)+10*log10(Pmin)) (dimensionless)
Col 30: SI Index on Sig1, numerator only: 10*log10(Pmax)-10*log10(Pmin) (dB)
Col 31: p on Sig1, spectral slope of detrended phase in the 0.1 to 25Hz range (dimensionless)
Col 32: Average Sig2 C/N0 over the last minute (dB-Hz)
Col 33: Total S4 on Sig2 (dimensionless)
Col 34: Correction to total S4 on Sig2 (thermal noise component only) (dimensionless)
Col 35: Phi01 on Sig2,  1-second phase sigma (radians)
Col 36: Phi03 on Sig2,  3-second phase sigma (radians)
Col 37: Phi10 on Sig2, 10-second phase sigma (radians)
Col 38: Phi30 on Sig2, 30-second phase sigma (radians)
Col 39: Phi60 on Sig2, 60-second phase sigma (radians)
Col 40: AvgCCD on Sig2, average of code/carrier divergence (meters)
Col 41: SigmaCCD on Sig2, standard deviation of code/carrier divergence (meters)
Col 42: Sig2 lock time (seconds)
Col 43: SI Index on Sig2 (dimensionless)
Col 44: SI Index on Sig2, numerator only (dB)
Col 45: p on Sig2, phase spectral slope in the 0.1 to 25Hz range (dimensionless)
Col 46: Average Sig3 C/N0 over the last minute (dB-Hz)
Col 47: Total S4 on Sig3 (dimensionless)
Col 48: Correction to total S4 on Sig3 (thermal noise component only) (dimensionless)
Col 49: Phi01 on Sig3,  1-second phase sigma (radians)
Col 50: Phi03 on Sig3,  3-second phase sigma (radians)
Col 51: Phi10 on Sig3, 10-second phase sigma (radians)
Col 52: Phi30 on Sig3, 30-second phase sigma (radians)
Col 53: Phi60 on Sig3, 60-second phase sigma (radians)
Col 54: AvgCCD on Sig3, average of code/carrier divergence (meters)
Col 55: SigmaCCD on Sig3, standard deviation of code/carrier divergence (meters)
Col 56: Sig3 lock time (seconds)
Col 57: SI Index on Sig3 (dimensionless)
Col 58: SI Index on Sig3, numerator only (dB)
Col 59: p on Sig3, phase spectral slope in the 0.1 to 25Hz range (dimensionless)
Col 60: T on Sig1, phase power spectral density at 1 Hz (rad^2/Hz)
Col 61: T on Sig2, phase power spectral density at 1 Hz (rad^2/Hz)
Col 62: T on Sig3, phase power spectral density at 1 Hz (rad^2/Hz)
"""
