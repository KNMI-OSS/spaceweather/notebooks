#! /usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Script to read and verify SW-advisories.
Author: M.L. Kooreman
'''

import sys
from glob import glob
import numpy as np
from datetime import datetime, timedelta
import csv
import warnings


def cleantxt(txt):
    return txt.split(':')[-1].strip()


fPath = '/Users/eelco/researchdrive/Space Weather Services/datasets/icao_advisories/'
fList = sorted(glob(fPath+'fnxx01*.txt'))
#print("flist", fList)

header = ['Arrival number',
          'Header type',
          'Header center',
          'Header time',
          'STATUS',
          'DTG',
          'SWXC',
          'ADVISORY NR',
          'NR RPLC',
          'SWX EFFECT',
          'SWX severity',
          'OBS SWX',
          'FCST SWX +6 HR',
          'FCST SWX +12 HR',
          'FCST SWX +18 HR',
          'FCST SWX +24 HR',
          'RMK',
          'NXT ADVISORY']

arr_nr = 0
fnxx_list = ['FNXX01', 'FNXX02', 'FNXX03', 'FNXX04']
station_list = ['YMMC', 'LFPW', 'ZBBB', 'UUAG', 'EFKL', 'EGRR', 'KWNP']

# Set parameters
status = 'VOID'
nr_rplc = 'VOID'

outf = open('advisories.csv', 'w', encoding='UTF8', newline='')
writer = csv.writer(outf)
writer.writerow(header)

for file in fList:
    for line in open(file):

        if line.startswith('ZCZC'):
            # new advisory: reset parameters
            status = 'VOID'
            nr_rplc = 'VOID'
            arr_nr += 1

        if line.startswith('FNXX'):
            fnxx, station, time = line.split()
            if fnxx not in fnxx_list:
                warnings.warn('Unknown FNXX number...')
            if station not in station_list:
                warnings.warn('Unknown station...')

        if line.startswith('SWX ADVISORY'):
            pass

        if line.startswith('STATUS:'):
            status = cleantxt(line)
            if status != 'TEST':
                status = 'NULL'

        if line.startswith('DTG:'):
            dtg = cleantxt(line)

        if line.startswith('SWXC:'):
            center = cleantxt(line)

        if line.startswith('ADVISORY NR:'):
            nr = cleantxt(line)

        if line.startswith('NR RPLC:'):
            nr_rplc = cleantxt(line)

        if line.startswith('SWX EFFECT:'):
            swa = cleantxt(line)
            sev = swa.split()[-1]
            swx = swa.rsplit(' ', 1)[0]

        if line.startswith('OBS SWX:'):
            obs = cleantxt(line)

        if line.startswith('FCST SWX  +6 HR:'):
            fc06 = cleantxt(line)
        if line.startswith('FCST SWX +6 HR:'):
            fc06 = cleantxt(line)

        if line.startswith('FCST SWX +12 HR:'):
            fc12 = cleantxt(line)

        if line.startswith('FCST SWX +18 HR:'):
            fc18 = cleantxt(line)

        if line.startswith('FCST SWX +24 HR:'):
            fc24 = cleantxt(line)

        if line.startswith('RMK:'):
            rmk = cleantxt(line)
        if line.startswith(' '):
            rmk += ' ' + cleantxt(line)

        if line.startswith('NXT ADVISORY:'):
            nxt = cleantxt(line)

        if line.startswith('NNNN'):
            writer.writerow([arr_nr, fnxx, station, time, status, dtg, center, nr, nr_rplc, swx, sev, obs, fc06, fc12, fc18, fc24, rmk, nxt])

            #reset parameters
            status = 'VOID'
            nr_rplc = 'VOID'
outf.close()

