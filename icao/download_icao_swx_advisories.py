import boto3
import os

local_path = f"{os.environ['HOME']}/SynologyDrive/datasets/icao_advisories/"
bucket_name = 'knmi-100-day-archive-prd'
prefix_name = 'verificatie/verifdata/spaceweather/'

session = boto3.Session(profile_name='knmi-backup-saml')
s3_client = session.client('s3')
filelist = s3_client.list_objects_v2(Bucket=bucket_name, 
                                     Prefix=prefix_name)

for fileobj in filelist['Contents']:
    download = False
    filename = os.path.basename(fileobj['Key'])
    local_filepath = f"{local_path}{filename}"
    remote_size = fileobj['Size']
    if os.path.isfile(local_filepath):
        local_size = os.path.getsize(local_filepath)
        if remote_size > local_size:
            download = True
    else:
        download = True
    if download:
        print(f"Downloading {fileobj['Key']}")
        with open(local_filepath, 'wb') as fh:
            s3_client.download_fileobj(bucket_name, fileobj['Key'], fh)
