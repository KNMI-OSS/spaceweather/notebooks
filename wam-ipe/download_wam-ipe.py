#!/usr/bin/env python3
import requests
from bs4 import BeautifulSoup
import os
import pandas as pd
from parfive import Downloader

def ensure_data_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)
    return directory

def mirror_wam_ipe(root_url, root_dir):
    dl = Downloader(max_conn=1)
    r = requests.get(root_url)
    if r.ok:
        soup = BeautifulSoup(r.text, "html.parser")
        dirs = []
        # Find the URLs in the table
        for pre in soup.find_all('pre'):
            for link in pre.find_all('a'):
                address = link.get('href')
                if address[0:1] != '/' and address[-1] == '/': # Directory
                    print(f"{address} is a directory")
                    ensure_data_dir(f"{root_dir}{address}")
                    mirror_wam_ipe(f"{root_url}{address}", f"{root_dir}{address}") # Call recursively
                if (address[0:1] != '?' and address[0:1] != '/' and address[-1] != '/'): # Get rid of the sorting options and previous directory links
                    url = f"{root_url}{address}"
                    download = False
                    if not os.path.isfile(f"{root_dir}{os.path.basename(url)}"): # Download if file does not exist
                         download = True                       
                    else:
                        print(f"Skipping {url} - already downloaded")

                    # if file size on the server is different
                    #    download = True
                    if download:
                        dl.enqueue_file(url, root_dir)
                        print(f"Enqueueing {url}")

    downloads = dl.download()
    for error in downloads.errors:
        print("=====")
        file_to_delete = f"{error[0].args[0]}/{os.path.basename(error[1])}"
        print("Error downloading: ", file_to_delete)
        print("URL:               ", error[1])      
        if os.path.isfile(file_to_delete):
            print("removing: ", file_to_delete)
            os.remove(file_to_delete)
    print("=====")

    return downloads

mirror_wam_ipe('https://nomads.ncep.noaa.gov/pub/data/nccf/com/wfs/prod/', './')
