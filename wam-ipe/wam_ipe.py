class wam_ipe():
    variable_collections = {'gsm10': ["lon", 
                                      "lat", 
                                      "height", 
                                      "temp_neutral", 
                                      "O_Density", 
                                      "O2_Density", 
                                      "N2_Density", 
                                      "u_neutral", 
                                      "v_neutral", 
                                      "w_neutral"],
                            'gsm05': ["lon", 
                                      "lat", 
                                      "den400", 
                                      "ON2"],
                            'ipe10': ["lon",
                                      "lat",
                                      "alt",
                                      "O_plus_density",
                                      "H_plus_density",
                                      "He_plus_density",
                                      "N_plus_density",
                                      "NO_plus_density",
                                      "O2_plus_density",
                                      "N2_plus_density",
                                      "ion_temperature",
                                      "electron_temperature",
                                      "eastward_exb_velocity",
                                      "northward_exb_velocity",
                                      "upward_exb_velocity"],
                            'ipe05': ["lon",
                                      "lat",
                                      "alt",
                                      "tec",
                                      "NmF2",
                                      "HmF2"]}

    collection_variables = {}
    for key in variable_collections:
        for var in variable_collections[key]:
            if var not in ['lon', 'lat', 'height']:
                collection_variables[var] = key

    def __init__(self, root_dir = '/Volumes/datasets/wam-ipe/', cache_dir = os.environ['HOME'] + '/tmp/wam-ipe/'):
        runs = []
        # List the run directories
        # run dir paths look like this: /Volumes/datasets/wam-ipe/wfs.20211123/00
        run_dirs = sorted(glob.glob(f"{root_dir}wfs*/*"))
        for run_dir in run_dirs:
            run_dir = run_dir
            run_cache_dir = cache_dir + run_dir.replace(root_dir, '') + '/'
            path_pieces = run_dir.split("/")
            rundate = pd.to_datetime(path_pieces[-2].split(".")[-1]) # Get the run date
            runtime = pd.to_timedelta(int(path_pieces[-1]), unit='H') # Get the run time in hours

            # Now get the start and end time from the tar file names contained in the run dir
            # tar file paths look like this "/Volumes/datasets/wam-ipe/wfs.20211123/00/wfs.t00z.20211122_21.tar"
            # get the simulation start and end times from these file names
            tar_files = sorted(glob.glob(run_dir + '/*'))
            (date, hour) = tar_files[0].split("/")[-1].split(".")[-2].split("_")
            start_time = pd.to_datetime(date) + pd.to_timedelta(int(hour), 'H')
            (date, hour) = tar_files[-1].split("/")[-1].split(".")[-2].split("_")
            end_time = pd.to_datetime(date) + pd.to_timedelta(int(hour), 'H')

            # Construct the record
            runs.append({'run_datetime': pd.to_datetime(rundate+runtime), 
                         'run_dir': run_dir + '/',
                         'cache_dir': run_cache_dir,
                         'start_time': start_time,
                         'end_time': end_time})

        self.runs = pd.DataFrame(runs)
        self.root_dir = root_dir
        self.cache_dir = cache_dir

        
    def runs_for_time(self, time):
        frame = self.runs[(time > self.runs['start_time']) & (time < self.runs['end_time'])]
        frame = frame.assign(time_delta = (time - frame.loc[:, 'run_datetime'])/pd.to_timedelta(1, 'H'))
        return frame

    
    def unpack_run(self, run_index):
        run = self.runs.loc[run_index]
        run_dir = run['run_dir']
        cache_dir = run['cache_dir'] 
        tar_files = sorted(glob.glob(run_dir + '/*.tar'))
        for tar_file in tar_files:
            print("Extracting " + tar_file)
            try:
                with open(tar_file, 'rb') as fh:
                    tf = tarfile.TarFile(fileobj = fh)
                    filenames = tf.getnames()
                    # print(filenames)
                    # ncfiles = [filename for filename in filenames if ('.ipe10.' in filename)]
                    # print(ncfiles)
                    for ncfile in filenames:
                        if not os.path.isfile(f"{cache_dir}{ncfile}"):
                            tf.extract(ncfile, path=cache_dir)
            except tarfile.ReadError:
                print(f"tarfile read error on {tar_file}")



